﻿using Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using CsvHelper;
using NLog;

namespace Extractor
{
	public class Program
	{
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

		static void Main(string[] args)
		{
			var repositoryUrls = ReadInputCsv(ConfigurationManager.AppSettings["inputFile"]);

			var outputPath = Path.ChangeExtension(
				ConfigurationManager.AppSettings["outputFile"],
				"." + DateTime.Now.ToString("s").Replace(':', '-') + ".csv");
			var writer = File.AppendText(outputPath);
			writer.AutoFlush = true;

			var csvOutput = new CsvWriter(writer);
			csvOutput.Configuration.CultureInfo = CultureInfo.InvariantCulture;
			csvOutput.Configuration.Delimiter = ";";

			var isFirstRecord = true;
			IList<string> headers = new List<string>();

			foreach (var url in repositoryUrls)
			{
				OutputRecordDto processedRepository;
				try
				{
					processedRepository = new OutputRecordDto
					{
						Url = url,
						Quantifiers =
							ExtractorFactory
								.GetExtractorFor(url)
								.Result
								.ExtractQuantifiers()
								.Result
					};
				}
				catch (Exception e)
				{
					Logger.Fatal($"Extraction failed for {url}");
					Logger.Error(e);
					continue;
				}

				if (isFirstRecord)
				{
					WriteHeaders(csvOutput, processedRepository, out headers);
					isFirstRecord = false;
				}
				
				csvOutput.WriteField<string>(processedRepository.Url);
				foreach (var header in headers.Skip(1))
				{
					WriteQuantifierByName(csvOutput, processedRepository, header);
				} 

				csvOutput.NextRecord();
			}
			
			Logger.Info($"Output has been written to \"{outputPath}\".");
			Console.ReadKey();
		}

		private static void WriteQuantifierByName(ICsvWriter csvOutput, OutputRecordDto processedRepository, string name)
		{
			if (csvOutput == null) throw new ArgumentNullException(nameof(csvOutput));
			if (processedRepository == null) throw new ArgumentNullException(nameof(processedRepository));
			if (string.IsNullOrWhiteSpace(name)) throw new ArgumentNullException(nameof(name));

			if (processedRepository.Quantifiers.All(q => q.Name != name)) return;
			var quantifier = processedRepository.Quantifiers.SingleOrDefault(q => q.Name == name);

			csvOutput.WriteField<string>(quantifier.Value.ToString());

			if (!quantifier.IsFaulted) return;
			
			Logger.Warn($"Quantifier {quantifier.Name} of repository {processedRepository.Url} has been faulted!");
			Logger.Debug(quantifier.FaultMessage);
		}

		private static void WriteHeaders(ICsvWriter csvOutput, OutputRecordDto newRecord, out IList<string> headers)
		{
			if (csvOutput == null) throw new ArgumentNullException(nameof(csvOutput));
			if (newRecord == null) throw new ArgumentNullException(nameof(newRecord));

			headers = new[]
				{
					nameof(newRecord.Url)
				}.Concat(newRecord.Quantifiers.Select(q => q.Name))
				.ToList();

			foreach (var header in headers)
			{
				csvOutput.WriteField(header);
			}

			csvOutput.NextRecord();
		}

		private static IEnumerable<string> ReadInputCsv(string inputFilePath)
		{
			if (string.IsNullOrWhiteSpace(inputFilePath)) throw new ArgumentNullException(nameof(inputFilePath));

			Logger.Info("Reading input... ");
			var csvInput = new CsvReader(File.OpenText(inputFilePath));
			var repositoryUrlDtos = csvInput.GetRecords<RepositoryUrlDto>().ToArray();
			var repositoryUrls = repositoryUrlDtos.Select(dto => dto.Url).Distinct().ToArray();
			Logger.Info($"{repositoryUrls.Length} distinct URL(s) found among {repositoryUrlDtos.Length} URL(s) originally present.");
			return repositoryUrls;
		}
	}
}
