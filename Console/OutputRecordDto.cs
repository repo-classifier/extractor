using System.Collections.Generic;
using Core.Explore;

namespace Extractor
{
	public class OutputRecordDto
	{
		public string Url { get; set; }
		public IEnumerable<Quantifier> Quantifiers { get; set; }
	}
}