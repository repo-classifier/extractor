﻿namespace Extractor
{
	public class RepositoryUrlDto
	{
		public string Url { get; set; }
	}
}
