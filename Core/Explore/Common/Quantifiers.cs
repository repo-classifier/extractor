﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Explore
{
	public class Quantifiers : IEnumerable<Quantifier>
	{
		private readonly ConcurrentDictionary<string, Quantifier> _quantifiers;

		public Quantifiers()
		{
			_quantifiers = new ConcurrentDictionary<string, Quantifier>();
		}

		public Quantifiers(IEnumerable<Quantifier> quantifiers)
		{
			if (quantifiers == null)
				throw new ArgumentNullException(nameof(quantifiers));

			_quantifiers = new ConcurrentDictionary<string, Quantifier>();
			foreach (var quantifier in quantifiers)
			{
				Add(quantifier);
			}
		}

		public void Add(Quantifier quantifier)
		{
			if (!_quantifiers.TryAdd(quantifier.Name, quantifier))
				throw new InvalidOperationException(
					$"Quantifier with name \"{quantifier.Name}\" already exists in the collection.");
		}

		public async Task AddAsync(Func<Quantifier> quantifierProvider)
		{
			if (quantifierProvider == null)
				throw new ArgumentNullException(nameof(quantifierProvider));

			await Task.Run(() => Add(quantifierProvider.Invoke()));
		}
		
		public async Task AddAsync(string name, Task<double?> valueProvider)
		{
			if (string.IsNullOrWhiteSpace(name)) throw new ArgumentNullException(nameof(name));
			if (valueProvider == null) throw new ArgumentNullException(nameof(valueProvider));

			try
			{
				var value = await valueProvider;
				await Task.Run(() => Add(new Quantifier(name, () => value)));
			}
			catch (Exception e)
			{
				await Task.Run(() => Add(new Quantifier(name, e.ToString())));
			}
		}

		public Quantifier this[string name]
		{
			get
			{
				if (_quantifiers.TryGetValue(name, out var quantifier))
					return quantifier;
				else throw new InvalidOperationException(
					$"Collection does not contain quantifier with name \"{name}\".");
			}
		}

		#region IEnumerable Members

		public IEnumerator<Quantifier> GetEnumerator()
		{
			return _quantifiers.Values.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return _quantifiers.GetEnumerator();
		}
	
		#endregion
	}
}
