﻿namespace Core.Explore
{
	internal interface IExplorer
	{
		void UpdateQuantifiers(Quantifiers quantifiers);
	}
}
