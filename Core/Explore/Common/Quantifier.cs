﻿using System;
using System.Diagnostics;

namespace Core.Explore
{
	public struct Quantifier
	{
		public string Name { get; private set; }
		public double? Value { get; private set; }
		public string FaultMessage { get; private set; }
		public bool IsFaulted => !string.IsNullOrWhiteSpace(FaultMessage);

		public Quantifier(string name, Func<double?> valueProvider)
		{
			if (string.IsNullOrWhiteSpace(name))
				throw new ArgumentNullException(nameof(name));

			if (valueProvider == null)
				throw new ArgumentNullException(nameof(valueProvider));

			Name = name;

			try
			{
				Value = valueProvider.Invoke();
				FaultMessage = string.Empty;
			}
			catch (Exception exception)
			{
				Value = default(double?);
				FaultMessage = exception.ToString();
			}
		}

		internal Quantifier(string name, string faultMessage)
		{
			if (string.IsNullOrWhiteSpace(name))
				throw new ArgumentNullException(nameof(name));

			if (string.IsNullOrWhiteSpace(faultMessage))
				throw new ArgumentNullException(nameof(faultMessage));

			Name = name;
			FaultMessage = faultMessage;
			Value = default(double?);
		}
	}
}
