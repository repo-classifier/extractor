﻿using Octokit;
using System.Linq;
using System.Threading.Tasks;
using Data;
using System;

namespace Core.Explore.Popularity
{
    internal class PopularityExplorer : IPopularityExplorer
    {
        private readonly Repository _repository;

        public PopularityExplorer(Repository repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public int GetForksCount()
        {
            return _repository.ForksCount;
        }

        public int GetStargazersCount()
        {
            return _repository.StargazersCount;
        }

        public async Task<int> GetCoreContributorsCountAsync()
        {
            using (var context = new DataContext())
            {
                var modificationSum = context.Commits.AsNoTracking().Sum(c => c.Additions + c.Deletions);
                //group commits by authors, and sort by commit modification size in descending order
                var contributions = context.Commits.AsNoTracking()
                  .GroupBy(commit => commit.AuthorEmail)
                  .Select(group =>
                      new
                      {
                          Author = group.Key,
                          RelativeContibution = group.Sum(commit => commit.Additions + commit.Deletions) * 1d / modificationSum
                      })
                  .OrderByDescending(contribution => contribution.RelativeContibution);

                //core contributors are those authors, whose modifications consitute up to 80% of all modifications made to the project
                var runningTotal = 0d;
                var coreContributors = await Task.Run(() =>
                    contributions.AsEnumerable() //gotta materialize for running total
                        .Select(c => new
                        {
                            Author = c.Author,
                            RunningTotal = runningTotal += c.RelativeContibution
                        })
                        .Where(c => c.RunningTotal < 0.8));

                return coreContributors.Count();
            }
        }        
    }
}
