﻿using Core.Extensions;
using Octokit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Core.Explore.ContinuousIntegration
{
	internal class ContinuousIntegrationExplorer : IExplorer
	{
		private readonly IGitHubClient _gitHubClient;
		private readonly Repository _repository;

		private readonly IEnumerable<ICICommitValidator> _ciValidators;

		/// <summary>
		/// Use <see cref="GetRepoBuildStatus"/> to access (lazily loaded with async request). 
		/// </summary>
		private IReadOnlyList<CommitStatus> _statuses;

		public ContinuousIntegrationExplorer(IGitHubClient gitHubClient, Repository repository)
		{
			_repository = repository ?? throw new ArgumentNullException(nameof(repository));
			_gitHubClient = gitHubClient ?? throw new ArgumentNullException(nameof(gitHubClient));

			_ciValidators = GetCICommitValidators();
		}

		public void UpdateQuantifiers(Quantifiers quantifiers)
		{
			Task.WaitAll(
				quantifiers.AddAsync("IsCIConfigured", GetCIConfigurationFlagAsync()),
				quantifiers.AddAsync("IsCIUsed", GetCIUsageFlagAsync()));
		}

		private async Task<double?> GetCIConfigurationFlagAsync()
		{
			var providers = GetCIProviders(await _repository.GetLocalAsync());
			var supportedProviders = _ciValidators.Select(validator => validator.ProviderName);

			return providers.Intersect(supportedProviders).Any()
				? 1 
				: (await GetRepoBuildStatus()).Any()
					? 1
					: 0;
		}

		private async Task<double?> GetCIUsageFlagAsync()
		{
			var localRepo = await _repository.GetLocalAsync();
			var hasVerifiedBuild = _ciValidators
				.Where(validator => GetCIProviders(localRepo).Contains(validator.ProviderName))
				.Select(async validator => await validator.CheckForAssociatedBuild(_repository, localRepo.Commits.First().Sha))
				.Any(flag => true);

			return hasVerifiedBuild
				? 1
				: (await GetRepoBuildStatus()).Any()
					? 1
					:0;
		}

		private IEnumerable<string> GetCIProviders(LibGit2Sharp.Repository localRepository)
		{
			if (localRepository == null) throw new ArgumentNullException(nameof(localRepository));

			return localRepository.Index
				.Where(item => item.Path.EndsWith(".yml"))
				.Select(item => Path.GetFileNameWithoutExtension(item.Path).Trim('.'));
		}

		private IEnumerable<ICICommitValidator> GetCICommitValidators()
		{
			return Assembly.GetExecutingAssembly().GetTypes()
				.Where(type => type.GetInterfaces().Contains(typeof(ICICommitValidator))
							&& type.IsClass
							&& !type.IsAbstract)
				.Select(type => Activator.CreateInstance(type) as ICICommitValidator)
				.AsEnumerable();
		}

		private async Task<IReadOnlyList<CommitStatus>> GetRepoBuildStatus(bool forceUpdate = false)
		{
			if (_statuses != null && !forceUpdate) return _statuses;

			_statuses = await _gitHubClient.Repository
				.Status
				.GetAll(_repository.Id, _repository.DefaultBranch);
			return _statuses;
		}
	}
}
