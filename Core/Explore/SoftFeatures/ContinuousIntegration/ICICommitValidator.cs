﻿using Octokit;
using System.Threading.Tasks;

namespace Core.Explore.ContinuousIntegration
{
    internal interface ICICommitValidator
    {
        string ProviderName { get; }
        Task<bool> CheckForAssociatedBuild(Repository repository, string sha);
    }
}
