﻿using Core.Auxiliary;
using Newtonsoft.Json;
using Octokit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NLog;

namespace Core.Explore.ContinuousIntegration.Travis
{
    internal class TravisCommitValidator : ICICommitValidator
    {
	    private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();

        private const string TravisAPIUrl = @"https://api.travis-ci.org";
        private const string TravisAcceptHeader = @"application/vnd.travis-ci.2+json";

        private const int MaxRequestPages = 5;

        public string ProviderName => "travis";

	    private async Task<BuildHistory> FetchBuildHistory(Repository repository, int? afterNumber = null)
        {
            if (repository == null) throw new ArgumentNullException(nameof(repository));

			try
	        {
		        var json = await new HttpRequestBuilder()
			        .WithUrl(TravisAPIUrl)
			        .WithPath("repos")
			        .WithPath(repository.FullName)
			        .WithPath("builds")
			        .WithUserAgentHeader("extractor")
			        .WithAcceptHeader(TravisAcceptHeader)
			        .WithOptionalParameter("after_number", afterNumber.ToString())
			        .BuildAndFetchResponseAsync();

		        return await Task.Run(() => JsonConvert.DeserializeObject<BuildHistory>(json));
	        }
	        catch (Exception e)
	        {
				Logger.Warn(e);
				return null;
	        }
        }

        public async Task<bool> CheckForAssociatedBuild(Repository repository, string sha)
        {
            if (repository == null) throw new ArgumentNullException(nameof(repository));
            if (string.IsNullOrWhiteSpace(sha)) throw new ArgumentNullException(nameof(sha));

            var buildHistory = await FetchBuildHistory(repository);
	        if (buildHistory == null)
		        return false;

            for (var i = 0; i < MaxRequestPages; i++)
            {
                if (buildHistory.Commits.Any(c => c.Sha == sha)) return true;
                var minBuildNumber = int.Parse(buildHistory.Builds.Min(build => build.Number));
                buildHistory = await FetchBuildHistory(repository, minBuildNumber);
            }

            return false;
        }
    }

}
