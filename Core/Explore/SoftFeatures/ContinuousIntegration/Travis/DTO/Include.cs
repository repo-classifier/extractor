﻿using Newtonsoft.Json;

namespace Core.Explore.ContinuousIntegration.Travis
{
    public class Include
    {
        [JsonProperty("os")]
        public string Os { get; set; }

        [JsonProperty("dist")]
        public string Dist { get; set; }

        [JsonProperty("sudo")]
        public string Sudo { get; set; }

        [JsonProperty("dotnet")]
        public string Dotnet { get; set; }

        [JsonProperty("osx_image")]
        public string OsxImage { get; set; }

        [JsonProperty("mono")]
        public string Mono { get; set; }
    }

}
