﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Core.Explore.ContinuousIntegration.Travis
{
    public class BuildHistory
    {
        [JsonProperty("builds")]
        public List<Build> Builds { get; set; }

        [JsonProperty("commits")]
        public List<Commit> Commits { get; set; }
    }
}
