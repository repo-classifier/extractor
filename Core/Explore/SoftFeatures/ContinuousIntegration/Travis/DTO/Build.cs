﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Core.Explore.ContinuousIntegration.Travis
{
    public class Build
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("repository_id")]
        public int RepositoryId { get; set; }

        [JsonProperty("commit_id")]
        public int CommitId { get; set; }

        [JsonProperty("number")]
        public string Number { get; set; }

        [JsonProperty("event_type")]
        public string EventType { get; set; }

        [JsonProperty("pull_request")]
        public bool PullRequest { get; set; }

        [JsonProperty("pull_request_title")]
        public string PullRequestTitle { get; set; }

        [JsonProperty("pull_request_number")]
        public int? PullRequestNumber { get; set; }

        [JsonProperty("config")]
        public Config Config { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("started_at")]
        public string StartedAt { get; set; }

        [JsonProperty("finished_at")]
        public string FinishedAt { get; set; }

        [JsonProperty("duration")]
        public int Duration { get; set; }

        [JsonProperty("job_ids")]
        public List<int> JobIds { get; set; }
    }

}
