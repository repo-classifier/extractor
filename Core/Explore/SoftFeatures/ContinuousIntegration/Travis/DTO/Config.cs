﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Core.Explore.ContinuousIntegration.Travis
{
    public class Config
    {
        [JsonProperty("language")]
        public string Language { get; set; }

        [JsonProperty("matrix")]
        public Matrix Matrix { get; set; }

        [JsonProperty("script")]
        public List<string> Script { get; set; }

        [JsonProperty(".result")]
        public string Result { get; set; }

        [JsonProperty("group")]
        public string Group { get; set; }

        [JsonProperty("dist")]
        public string Dist { get; set; }

        [JsonProperty("install")]
        public List<string> Install { get; set; }

        [JsonProperty("before_install")]
        public List<string> BeforeInstall { get; set; }
    }

}
