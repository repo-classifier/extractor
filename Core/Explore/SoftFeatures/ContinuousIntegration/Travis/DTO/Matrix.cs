﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Core.Explore.ContinuousIntegration.Travis
{
    public class Matrix
    {
        [JsonProperty("Include")]
        public List<Include> Include { get; set; }
    }

}
