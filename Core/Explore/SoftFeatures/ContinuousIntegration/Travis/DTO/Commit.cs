﻿using Newtonsoft.Json;

namespace Core.Explore.ContinuousIntegration.Travis
{
    public class Commit
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("sha")]
        public string Sha { get; set; }

        [JsonProperty("branch")]
        public string Branch { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("committed_at")]
        public string CommittedAt { get; set; }

        [JsonProperty("author_name")]
        public string AuthorName { get; set; }

        [JsonProperty("author_email")]
        public string AuthorEmail { get; set; }

        [JsonProperty("committer_name")]
        public string CommitterName { get; set; }

        [JsonProperty("committer_email")]
        public string CommitterEmail { get; set; }

        [JsonProperty("compare_url")]
        public string CompareUrl { get; set; }

        [JsonProperty("pull_request_number")]
        public int? PullRequestNumber { get; set; }
    }

}
