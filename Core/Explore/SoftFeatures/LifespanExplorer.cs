﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;

namespace Core.Explore
{
	internal class LifespanExplorer : IExplorer
	{
		private readonly IEnumerable<string> _adaptiveKeywords;
		private readonly IEnumerable<string> _correctiveKeywords;
		private readonly IEnumerable<string> _perfectiveKeywords;

		public LifespanExplorer(
			IEnumerable<string> adaptiveKeywords,
			IEnumerable<string> correctiveKeywords,
			IEnumerable<string> perfectiveKeywords)
		{
			_adaptiveKeywords = adaptiveKeywords ?? throw new ArgumentNullException(nameof(adaptiveKeywords));
			_correctiveKeywords = correctiveKeywords ?? throw new ArgumentNullException(nameof(correctiveKeywords));
			_perfectiveKeywords = perfectiveKeywords ?? throw new ArgumentNullException(nameof(perfectiveKeywords));
		}

		public void UpdateQuantifiers(Quantifiers quantifiers)
		{
			Task.WaitAll(
				quantifiers.AddAsync(() => 
					new Quantifier(
						"AverageAdaptationFrequency",
						GetAverageAdaptationFrequency)),
				quantifiers.AddAsync(() =>
					new Quantifier(
						"AverageCorrectionFrequency",
						GetAverageCorrectionFrequency)),
				quantifiers.AddAsync(() =>
					new Quantifier(
						"AveragePerfectionFrequency",
						GetAveragePerfectionFrequency)),
				quantifiers.AddAsync(() =>
					new Quantifier(
						"AverageUncategorizedChangeFrequency", 
						GetAverageUncategorizedChangeFrequency)),
				quantifiers.AddAsync(() =>
					new Quantifier(
						"AverageChangeFrequency", 
						GetAverageChangeFrequency)),
				quantifiers.AddAsync(() =>
					new Quantifier(
						"AverageSizeOfChangeFrequency", 
						GetAverageSizeOfChangeFrequency)));
		}

		private double? GetAverageAdaptationFrequency()
		{
			using (var context = new DataContext())
			{
				return context
					.Commits
					.AsNoTracking()
					.GroupBy(commit => new
					{
						commit.When.Year,
						commit.When.Month
					})
					.Select(grouping => new
					{
						Adaptive = grouping.Count(commit =>
							_adaptiveKeywords
								.Any(keyword => commit.Message.Contains(keyword)))
					})
					.Average(grouping => grouping.Adaptive);
			}
		}

		private double? GetAverageCorrectionFrequency()
		{
			using (var context = new DataContext())
			{
				return context
					.Commits
					.AsNoTracking()
					.GroupBy(commit => new
					{
						commit.When.Year,
						commit.When.Month
					})
					.Select(grouping => new
					{
						Corrective = grouping.Count(commit =>
							_correctiveKeywords
								.Any(keyword => commit.Message.Contains(keyword)))
					})
					.Average(grouping => grouping.Corrective);
			}
		}

		private double? GetAveragePerfectionFrequency()
		{
			using (var context = new DataContext())
			{
				return context
					.Commits
					.AsNoTracking()
					.GroupBy(commit => new
					{
						commit.When.Year,
						commit.When.Month
					})
					.Select(grouping => new
					{
						Perfective = grouping.Count(commit =>
							_perfectiveKeywords
								.Any(keyword => commit.Message.Contains(keyword)))
					})
					.Average(grouping => grouping.Perfective);
			}
		}

		private double? GetAverageUncategorizedChangeFrequency()
		{
			using (var context = new DataContext())
			{
				return context
					.Commits
					.AsNoTracking()
					.GroupBy(commit => new
					{
						commit.When.Year,
						commit.When.Month
					})
					.Select(grouping => new
					{
						Uncategorized = grouping.Count(commit =>
							!_adaptiveKeywords.Any(keyword => commit.Message.Contains(keyword)) &&
							!_correctiveKeywords.Any(keyword => commit.Message.Contains(keyword)) &&
							!_perfectiveKeywords.Any(keyword => commit.Message.Contains(keyword)))
					})
					.Average(grouping => grouping.Uncategorized);
			}
		}

		private double? GetAverageChangeFrequency()
		{
			using (var context = new DataContext())
			{
				return context
					.Commits
					.AsNoTracking()
					.GroupBy(commit => new
					{
						commit.When.Year,
						commit.When.Month
					})
					.Select(grouping => new
					{
						Count = grouping.Count()
					})
					.Average(grouping => grouping.Count);
			}
		}

		private double? GetAverageSizeOfChangeFrequency()
		{
			using (var context = new DataContext())
			{
				return context
					.Commits
					.AsNoTracking()
					.GroupBy(commit => new
					{
						commit.When.Year,
						commit.When.Month
					})
					.Select(grouping => new
					{
						Changes = grouping.Sum(commit => commit.Additions + commit.Deletions)
					})
					.Average(grouping => grouping.Changes);
			}
		}

	}
}
