﻿using Newtonsoft.Json;

namespace Core.Explore.License
{
    public class LicenseInfo
    {
        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("spdx_id")]
        public string SpdxId { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("featured")]
        public bool Featured { get; set; }
    }
}
