﻿using Core.Auxiliary;
using Newtonsoft.Json;
using Octokit;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using NLog;

namespace Core.Explore.License
{
	internal class LicenseExplorer : IExplorer
	{
		private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();

		private readonly Repository _repository;

		public LicenseExplorer(Repository repository)
		{
			_repository = repository ?? throw new ArgumentNullException(nameof(repository));
		}

		public void UpdateQuantifiers(Quantifiers quantifiers)
		{
			quantifiers.AddAsync("License", TryGetLicense()).Wait();
		}

		private async Task<RepositoryLicense> GetLicense()
		{
			var json = new HttpRequestBuilder()
				.WithUrl(_repository.Url + "/")
				.WithPath("license")
				.WithAcceptHeader("application / vnd.github.drax - preview + json")
				.WithUserAgentHeader("Anything")
				.BuildAndFetchResponseAsync();

			return JsonConvert.DeserializeObject<RepositoryLicense>(await json);
		}

		private async Task<double?> TryGetLicense()
		{
			try
			{
				return await GetLicense() != null
					? 1
					: 0;
			}
			catch (Exception)
			{
				return null;
			}
		}
	}
}
