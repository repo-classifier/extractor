﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Core.Extensions;
using Data;
using Microsoft.Isam.Esent.Interop;

namespace Core.Explore
{
	internal class IssueExplorer : IExplorer
	{
		public void UpdateQuantifiers(Quantifiers quantifiers)
		{
			Task.WaitAll(
				quantifiers.AddAsync(() =>
					new Quantifier(
						"IssueClosedCount",
						GetClosedIssueCount)),
				quantifiers.AddAsync(() =>
					new Quantifier(
						"IssueFrequency",
						GetIssueFrequency)),
				quantifiers.AddAsync(() =>
					new Quantifier(
						"IssueOpenCount",
						GetOpenIssueCount)));
		}

		public double? GetClosedIssueCount()
		{
			using (var context = new DataContext())
			{
				return context.Issues.Any() 
					? context.Issues.Count(issue => issue.ClosedAt.HasValue)
					: default(double?);
			}
		}

		public double? GetIssueFrequency()
		{
			using (var context = new DataContext())
			{
				if (!context.Issues.Any())
					return default(double?);

				var frequencies = context.Issues.AsNoTracking()
					.GroupBy(issue => new
					{
						issue.CreatedAt.Year,
						issue.CreatedAt.Month
					})
					.Select(grouping => new { NumberOfIssues = grouping.Count() });

				var standardDeviation = frequencies.StandardDeviation(f => f.NumberOfIssues);

				return frequencies
					.Where(f=>f.NumberOfIssues < 3 * standardDeviation)
					.Average(f => f.NumberOfIssues);
			}
		}

		public double? GetIssueSurvivalAsync()
		{
			using (var context = new DataContext())
			{
				if (!context.Issues.Any())
					return default(double?);

				var standardDeviation = context
					.Issues
					.AsNoTracking()
					.StandardDeviation(issue => issue.DaysSurvived);

				return context
					.Issues
						.Where(issue => issue.DaysSurvived < 3 * standardDeviation)
						.Average(issue => issue.DaysSurvived);
			}
		}


		public double? GetOpenIssueCount()
		{
			using (var context = new DataContext())
			{
				return context.Issues.Any()
					? context.Issues.Count(issue => !issue.ClosedAt.HasValue)
					: default(double?);
			}
		}
	}
}