﻿using Data;
using Octokit;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Explore
{
	internal class PopularityExplorer : IExplorer
	{
		private readonly Repository _repository;

		public PopularityExplorer(Repository repository)
		{
			_repository = repository ?? throw new ArgumentNullException(nameof(repository));
		}

		public void UpdateQuantifiers(Quantifiers quantifiers)
		{
			Task.WaitAll(
				quantifiers.AddAsync( () =>
						new Quantifier(
							"ForksCount",
							GetForksCount)),
				quantifiers.AddAsync(() =>
						new Quantifier(
							"StargazersCount",
							GetStargazersCount)),
				quantifiers.AddAsync(() =>
						new Quantifier(
							"CoreContributorsCount", 
							GetCoreContributorsCount)));
		}

		private double? GetForksCount()
		{
			return _repository.ForksCount;
		}

		private double? GetStargazersCount()
		{
			return _repository.StargazersCount;
		}

		private double? GetCoreContributorsCount()
		{
			using (var context = new DataContext())
			{
				var modificationSum = context.Commits.AsNoTracking().Sum(c => c.Additions + c.Deletions);
				//group commits by authors, and sort by commit modification size in descending order
				var contributions = context.Commits.AsNoTracking()
					.GroupBy(commit => commit.AuthorEmail)
					.Select(group =>
						new
						{
							Author = group.Key,
							RelativeContribution = group.Sum(commit => commit.Additions + commit.Deletions) * 1d / modificationSum
						})
					.OrderByDescending(contribution => contribution.RelativeContribution);

				//core contributors are those authors, whose modifications consitute up to 80% of all modifications made to the project
				var runningTotal = 0d;

				//take the contibutor with most contributions anyway
				var coreContributors =
					contributions
						.AsEnumerable()
						.Take(1)
						.Select(c => new
						{
							Author = c.Author,
							RunningTotal = runningTotal += c.RelativeContribution
						});

				//add contributors until 80 percent total is reached
				coreContributors = coreContributors
					.Concat(
						contributions
							.Skip(1)
							.AsEnumerable() 
							.Select(c => new
							{
								Author = c.Author,
								RunningTotal = runningTotal += c.RelativeContribution
							})
							.Where(c => c.RunningTotal < 0.8))
					.ToArray();
					
			
				return coreContributors.Count();
			}
		}

	}
}
