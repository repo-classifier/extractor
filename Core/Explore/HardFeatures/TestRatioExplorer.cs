﻿using Core.Auxiliary;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Core.Explore
{
	internal class TestRatioExplorer : IRoslynExplorer
	{
		private readonly IEnumerable<string> _classTestAttributes;
		private readonly IEnumerable<string> _methodTestAttributes;

		private int _classesCount = 0;
		private int _locCount = 0;
		private int _testClassesCount = 0;
		private int _testLocCount = 0;

		public TestRatioExplorer(IEnumerable<string> classTestAttributes,
			IEnumerable<string> methodTestAttributes)
		{
			_classTestAttributes = classTestAttributes ?? throw new ArgumentNullException(nameof(classTestAttributes));
			_methodTestAttributes = methodTestAttributes ?? throw new ArgumentNullException(nameof(methodTestAttributes));
		}

		public void CollectData(SyntaxNode syntaxRoot, SemanticModel semanticModel)
		{
			foreach (var classNode in syntaxRoot.DescendantNodes().OfType<ClassDeclarationSyntax>())
			{
				_classesCount++;

				var linesInClass = LineCounter.GetLinesCountFor(classNode);

				_locCount += linesInClass;

				if (HasTestAttributes(classNode.AttributeLists, _classTestAttributes)
					|| HasTestMethods(classNode))
				{
					_testClassesCount++;
					_testLocCount += linesInClass;
				}
			}
		}

		public void UpdateQuantifiers(Quantifiers quantifiers)
		{
			Task.WaitAll(
				quantifiers.AddAsync(() =>
					new Quantifier(
						"TestClassRatio",
						() => MathHelper.GetProportion(_testClassesCount, _classesCount))),
				quantifiers.AddAsync(() =>
					new Quantifier(
						"TestLinesRatio",
						() => MathHelper.GetProportion(_testLocCount, _locCount))));
		}

		private static bool HasTestAttributes(
			SyntaxList<AttributeListSyntax> attributeListNode,
			IEnumerable<string> knownTestAttributeNames)
		{
			return attributeListNode
				.SelectMany(attrListNode => attrListNode.Attributes)
				.Select(attr => attr.Name.GetText().ToString())
				.Any(knownTestAttributeNames.Contains);
		}

		private bool HasTestMethods(ClassDeclarationSyntax classNode)
		{
			return classNode
				.Members
				.OfType<MethodDeclarationSyntax>()
				.Any(methodNode =>
					HasTestAttributes(methodNode.AttributeLists, _methodTestAttributes));
		}
	}
}
