﻿using Microsoft.CodeAnalysis;
using System.Threading.Tasks;

namespace Core.Explore
{
	internal interface IRoslynExplorer : IExplorer
	{
		void CollectData(SyntaxNode syntaxRoot, SemanticModel semanticModel);
	}
}
