﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Auxiliary;
using Microsoft.CodeAnalysis;
using NLog;

namespace Core.Explore
{
	internal class RoslynExtractor
	{
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

		private readonly ISolutionContainer _solutionContainer;
		private readonly IList<IRoslynExplorer> _explorers;

		private RoslynExtractor(ISolutionContainer solutionContainer)
		{
			_solutionContainer = solutionContainer ?? throw new ArgumentNullException(nameof(solutionContainer));
			_explorers = new List<IRoslynExplorer>();
			Quantifiers = new Quantifiers();
		}

		public static Builder Initialize(ISolutionContainer solutionContainer)
		{
			if (solutionContainer == null)
				throw new ArgumentNullException(nameof(solutionContainer));

			return new Builder(solutionContainer);
		}

		public Quantifiers Quantifiers { get; }

		public void RegisterExplorer(IRoslynExplorer explorer)
		{
			if (explorer == null)
				throw new ArgumentNullException(nameof(explorer));

			_explorers.Add(explorer);
		}

		public async Task DoExtractionForEachDocument()
		{
			var totalNumberOfDocuments = _solutionContainer.Documents.Count();
			var numberOfDocumentsProcessed = 0;

			if (!_solutionContainer.Documents.Any())
			{
				Logger.Warn("No documents found!");
				return;
			}

			foreach (var document in _solutionContainer.Documents)
			{
				Logger.Info($"Processing document {document.Name} ");

				SyntaxNode syntaxRoot;
				try
				{
					syntaxRoot = await document.GetSyntaxRootAsync();
					Logger.Trace("Syntax tree aqcuired.");
				}
				catch (Exception e)
				{
					Logger.Error(e);
					continue;
				}

				SemanticModel semanticModel;
				try
				{
					semanticModel = await document.GetSemanticModelAsync();
					Logger.Trace("Semantic model aqcuired.");
				}
				catch (Exception e)
				{
					Logger.Error(e);
					continue;
				}
				
				Logger.Trace("Extracting hard data.");
				Parallel.ForEach(
					_explorers,
					explorer =>
					{
						(explorer as IDocumentExplorer)?.ExploreDocument(document);
						explorer.CollectData(syntaxRoot, semanticModel);
					});
				Logger.Trace("Extraction of hard data complete!");

				numberOfDocumentsProcessed += 1;
				Logger.Info($"{MathHelper.GetProportion(numberOfDocumentsProcessed, totalNumberOfDocuments):P} of repository processed.");
			}

			Logger.Trace("Extracting hard quantifiers...");

			Parallel.ForEach(
					_explorers,
					explorer =>
						explorer.UpdateQuantifiers(Quantifiers));

			Logger.Info("Hard quantifiers extracted.");
		}

		internal class Builder
		{
			private readonly RoslynExtractor _extractor;

			internal Builder(ISolutionContainer solutionContainer)
			{
				if (solutionContainer == null)
					throw new ArgumentNullException(nameof(solutionContainer));

				_extractor = new RoslynExtractor(solutionContainer);
			}

			public Builder With(IRoslynExplorer explorer)
			{
				if (explorer == null)
					throw new ArgumentNullException(nameof(explorer));

				_extractor.RegisterExplorer(explorer);
				return this;
			}

			public RoslynExtractor Build()
			{
				return _extractor;
			}
		}
	}
}
