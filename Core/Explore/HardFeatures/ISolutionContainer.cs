﻿using System.Collections.Generic;
using LibGit2Sharp;
using Microsoft.CodeAnalysis;

namespace Core.Explore
{
	internal interface ISolutionContainer
	{
		IEnumerable<Document> Documents { get; }
		IEnumerable<Project> Projects { get; }
		IEnumerable<Solution> Solutions { get; }
	}
}