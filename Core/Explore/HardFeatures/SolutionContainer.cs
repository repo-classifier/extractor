﻿using LibGit2Sharp;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.MSBuild;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Core.Auxiliary;

namespace Core.Explore
{
	internal class SolutionContainer : ISolutionContainer
	{
		private readonly ICollection<Solution> _solutions = new List<Solution>();

		public IEnumerable<Solution> Solutions => _solutions;

		public IEnumerable<Project> Projects => _solutions.SelectMany(solution => solution.Projects);

		public IEnumerable<Document> Documents => Projects.SelectMany(project => project.Documents);
		
		private SolutionContainer()
		{
		}

		public static async Task<ISolutionContainer> Instance(Repository repository)
		{
			if (repository == null)
				throw new ArgumentNullException(nameof(repository));
					
			var container = new SolutionContainer();
			await container.Load(repository);

			return container;
		}

		private async Task Load(Repository repository)
		{
			if (repository == null)
				throw new ArgumentNullException(nameof(repository));

			foreach (var solutionPath in GetSolutionPaths(repository))
			{
				_solutions.Add(await MSBuildWorkspace.Create().OpenSolutionAsync(solutionPath));
			}
		}		

		private bool AreSolutionsLoaded(Repository repository)
		{
			if (repository == null)
				throw new ArgumentNullException(nameof(repository));

			if (!_solutions.Any())
				return false;

			return GetSolutionPaths(repository)
				.OrderBy(path => path)
				.SequenceEqual(
					_solutions
						.Select(solution => solution.FilePath)
						.OrderBy(path => path));
		}
		
		private static IEnumerable<string> GetSolutionPaths(Repository repository)
		{
			if (repository == null)
				throw new ArgumentNullException(nameof(repository));

			return repository
				.Index
				.Where(item => item.Path.EndsWith(".sln"))
				.Select(item => Path.Combine(repository.Info.WorkingDirectory, item.Path));
		}		
	}
}
