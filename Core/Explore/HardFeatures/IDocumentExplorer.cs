﻿using Microsoft.CodeAnalysis;

namespace Core.Explore
{
	internal interface IDocumentExplorer: IRoslynExplorer
	{
		void ExploreDocument(Document document);
	}
}
