﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Core.Auxiliary;

namespace Core.Explore
{
	internal class CommentRatioExplorer : IRoslynExplorer
	{
		private readonly List<double> _commentRatios;
		private int _sourceCodeLinesTotal = 0;
		private int _commentLinesCountTotal = 0;

		public CommentRatioExplorer()
		{
			_commentRatios = new List<double>();
		}
		
		public void CollectData(SyntaxNode syntaxRoot, SemanticModel semanticModel)
		{			
			var loc = LineCounter.GetLinesCountFor(syntaxRoot);
			var cloc = GetCommentLinesCount(syntaxRoot);

			_sourceCodeLinesTotal += loc;
			_commentLinesCountTotal += cloc;
			_commentRatios.Add(MathHelper.GetProportion(cloc, loc));			
		}

		public void UpdateQuantifiers(Quantifiers quantifiers)
		{
			Task.WaitAll(
				quantifiers.AddAsync(() =>
					new Quantifier(
						"CommentRatioPerDocument",
						() => _commentRatios.Average())),
				quantifiers.AddAsync(() =>
					new Quantifier(
						"CommentRatioPerSolution",
						() => MathHelper.GetProportion(_commentLinesCountTotal, _sourceCodeLinesTotal))));
		}

		private static int GetCommentLinesCount(SyntaxNode root)
		{
			var singleLineCommentCount = root
				.DescendantTrivia()
				.Where(trivia => trivia.Kind() == SyntaxKind.SingleLineCommentTrivia)
				.Distinct()
				.Count();

			var xmlDocumentationLineCount = root
				.DescendantTrivia()
				.Where(trivia => trivia.Kind() == SyntaxKind.SingleLineDocumentationCommentTrivia)
				.SelectMany(c => c.GetStructure().DescendantTokens())
				.Count(token => token.Kind() == SyntaxKind.XmlTextLiteralNewLineToken);

			return singleLineCommentCount + xmlDocumentationLineCount;
		}
	}
}