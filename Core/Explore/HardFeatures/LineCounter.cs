﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using System.Linq;

namespace Core.Explore
{
	internal static class LineCounter 
	{
		public static int GetLinesCountFor(SyntaxNode root)
		{
			var endOfLineTriviaCount = root
				.DescendantNodesAndTokens()
				.Where(nodeOrToken => !nodeOrToken.ChildNodesAndTokens().Any())
				.SelectMany(nodeOrToken => nodeOrToken.GetTrailingTrivia())
				.Count(trivia => trivia.Kind() == SyntaxKind.EndOfLineTrivia);
			
			if (!TryGetEndOfFileToken(root, out var endOfFileToken))
				return endOfLineTriviaCount;

			// the last code-related token may not contain EoL trivia
			// (in case it is followed by EoF token)

			var lastBeforeEoFToken = root
				.DescendantTokens()
				.SingleOrDefault(token =>
					token.Span.End == endOfFileToken.SpanStart &&
					token.Kind() != SyntaxKind.EndOfFileToken);

			if (lastBeforeEoFToken != default(SyntaxToken))
				endOfLineTriviaCount++;

			return endOfLineTriviaCount;
		}

		private static bool TryGetEndOfFileToken(SyntaxNode node, out SyntaxToken endOfFileToken)
		{
			endOfFileToken = node
				.DescendantTokens()
				.SingleOrDefault(token =>
					token.Kind() == SyntaxKind.EndOfFileToken);

			return endOfFileToken != default(SyntaxToken);
		}
	}
}
