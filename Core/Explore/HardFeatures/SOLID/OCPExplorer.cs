﻿using Core.Auxiliary;
using Core.Extensions;
using LibGit2Sharp;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Explore
{
	internal class OCPExplorer : IDocumentExplorer
	{
		private int _classCount = 0;
		private int _abstractionCount = 0;
		private Octokit.Repository _repository;
		private List<double> _hunkAges;

		public OCPExplorer(Octokit.Repository repository)
		{
			_repository = repository ?? throw new ArgumentNullException(nameof(repository));
			_hunkAges = new List<double>();
		}

		public void CollectData(SyntaxNode syntaxRoot, SemanticModel semanticModel)
		{
			if (syntaxRoot == null)
				throw new ArgumentNullException(nameof(syntaxRoot));

			if (semanticModel == null)
				throw new ArgumentNullException(nameof(semanticModel));

			Task.WaitAll(
				Task.Run(() => CollectClassCount(syntaxRoot, semanticModel)),
				Task.Run(() => CollectAbstractionCount(syntaxRoot, semanticModel)));
		}

		public void UpdateQuantifiers(Quantifiers quantifiers)
		{
			if (quantifiers == null)
				throw new ArgumentNullException(nameof(quantifiers));


			Task.WaitAll(
				quantifiers.AddAsync(() => 
						new Quantifier(
							"AbstractionRatio",
							() => MathHelper.GetProportion(_abstractionCount, _classCount))),
					quantifiers.AddAsync(() =>
						new Quantifier(
							"AverageCodeLongevity",
							() => _hunkAges.Any()
								? _hunkAges.Average()
								: default(double?))),
					quantifiers.AddAsync(() =>
						new Quantifier(
							"MaxCodeLongevity",
							() => _hunkAges.Any()
								? _hunkAges.Max()
								: default(double?))));
		}

		public void ExploreDocument(Document document)
		{
			CollectCodeLongevity(document);
		}

		private void CollectClassCount(SyntaxNode syntaxRoot, SemanticModel semanticModel)
		{
			if (syntaxRoot == null)
				throw new ArgumentNullException(nameof(syntaxRoot));

			if (semanticModel == null)
				throw new ArgumentNullException(nameof(semanticModel));

			_classCount += syntaxRoot
				.DescendantNodes()
				.OfType<ClassDeclarationSyntax>()
				.Count(classNode => !semanticModel
					.GetDeclaredSymbol(classNode)
						?.IsAbstract
						?? false);
		}

		private void CollectAbstractionCount(SyntaxNode syntaxRoot, SemanticModel semanticModel)
		{
			if (syntaxRoot == null)
				throw new ArgumentNullException(nameof(syntaxRoot));

			if (semanticModel == null)
				throw new ArgumentNullException(nameof(semanticModel));

			_abstractionCount += syntaxRoot
				.DescendantNodes()
				.OfType<TypeDeclarationSyntax>()
				.Count(classNode => semanticModel
					.GetDeclaredSymbol(classNode)
						?.IsAbstract
						?? false);
		}

		private void CollectCodeLongevity(Document document)
		{
			if (document == null)
				throw new ArgumentNullException(nameof(document));

			var localRepository = _repository.GetLocalAsync().Result;

			var documentRelativePath = document
				.FilePath
				.Replace(
					localRepository.Info.WorkingDirectory,
					string.Empty);

			if (!localRepository
					.Index
					.Any(indexEntry => 
						indexEntry.Path == documentRelativePath))
				return;

			_hunkAges.Add(
				localRepository
					.Blame(documentRelativePath)
					.Select(hunk =>
						(DateTime.UtcNow - hunk.FinalCommit.Committer.When.UtcDateTime)
						.Days)
					.ToArray()
					.Average());
		}


	}
}
