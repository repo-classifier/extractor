﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Explore
{
	class SRPExporer : IRoslynExplorer
	{
		private readonly List<(int LinesCount, int MembersCount)> _classWeights;

		public SRPExporer()
		{
			_classWeights = new List<(int, int)>();
		}

		public void CollectData(SyntaxNode syntaxRoot, SemanticModel semanticModel)
		{
			CollectClassWeights(syntaxRoot, semanticModel);
		}

		public void UpdateQuantifiers(Quantifiers quantifiers)
		{
			if (quantifiers == null)
				throw new ArgumentNullException(nameof(quantifiers));

			Task.WaitAll(
				quantifiers.AddAsync(() =>
					new Quantifier(
						"AverageClassWeight",
						() => _classWeights.Any()
							? _classWeights.Average(weight => weight.LinesCount)
							: default(double?))),
				quantifiers.AddAsync(() =>
					new Quantifier(
						"AverageClassStructuralWeight",
						() => _classWeights.Any()
							? _classWeights.Average(weight => weight.MembersCount)
							: default(double?))),
				quantifiers.AddAsync(() =>
					new Quantifier(
						"ClassCount",
						() => _classWeights.Any()
							? _classWeights.Count()
							: default(double?))));
		}

		private void CollectClassWeights(SyntaxNode documentRoot, SemanticModel documentSemantics)
		{
			if (documentRoot == null)
				throw new ArgumentNullException(nameof(documentRoot));

			if (documentSemantics == null)
				throw new ArgumentNullException(nameof(documentSemantics));

			_classWeights.AddRange(documentRoot
					.DescendantNodes()
						.OfType<ClassDeclarationSyntax>()
						.Where(classNode => !documentSemantics.GetDeclaredSymbol(classNode)?.IsAbstract ?? false)
						.Select(classNode => (LineCounter.GetLinesCountFor(classNode), classNode.Members.Count)));
		}
	}
}
