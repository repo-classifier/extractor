﻿using System;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Threading.Tasks;
using Core.Auxiliary;
using System.Collections.Generic;

namespace Core.Explore
{
	internal class  DIPExplorer : IRoslynExplorer
	{
		private List<double?> _averageAbstractionRatiosPerMethod;
		private List<double?> _averageAbstractionRatiosPerConstructor;

		public DIPExplorer()
		{
			_averageAbstractionRatiosPerMethod = new List<double?>();
			_averageAbstractionRatiosPerConstructor = new List<double?>();
		}

		public void CollectData(SyntaxNode syntaxRoot, SemanticModel semanticModel)
		{
			_averageAbstractionRatiosPerMethod
					.Add(
						GetParameterAbstractionDegree(
							semanticModel,
							syntaxRoot.DescendantNodes()
								.OfType<MethodDeclarationSyntax>()));

			_averageAbstractionRatiosPerConstructor
				.Add(
					GetParameterAbstractionDegree(
						semanticModel,
						syntaxRoot.DescendantNodes()
							.OfType<ConstructorDeclarationSyntax>()));
		}
		
		public void UpdateQuantifiers(Quantifiers quantifiers)
		{
			Task.WaitAll(
				quantifiers.AddAsync(() =>
				new Quantifier(
					"AverageAbstractionRatioPerMethod",
					() => _averageAbstractionRatiosPerMethod.Any()
						? _averageAbstractionRatiosPerMethod.Average()
						: 0)),
				quantifiers.AddAsync(() =>
				new Quantifier(
					"AverageAbstractionRatioPerConstructor",
					() => _averageAbstractionRatiosPerConstructor.Any()
						? _averageAbstractionRatiosPerConstructor.Average()
						: 0)));
		}

		private double? GetParameterAbstractionDegree(
			SemanticModel model,
			IEnumerable<BaseMethodDeclarationSyntax> declarations)
		{
			if (model == null)
				throw new ArgumentNullException(nameof(model));

			if (declarations == null)
				throw new ArgumentNullException(nameof(declarations));

			var parameterLists = declarations
				.Select(method => method
					.ParameterList
					.DescendantNodes()
					.OfType<IdentifierNameSyntax>()
					.Select(identifier => model.GetTypeInfo(identifier).Type)
					.Where(typeInfo => typeInfo?.Kind == SymbolKind.NamedType))
				.Where(parameterTypesList => parameterTypesList.Any());

			return parameterLists.Any()
				? parameterLists.Average(parameterList =>
					MathHelper.GetProportion(
						parameterList.Count(parameter => parameter.IsAbstract),
						parameterList.Count()))
				: default(double?);			
		}
	}
}
