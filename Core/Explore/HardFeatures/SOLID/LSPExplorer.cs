﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace Core.Explore
{
	internal class LSPExplorer : IRoslynExplorer
	{
		private const string StubExceptionName = "NotImplementedException";

		private List<int> _classInheritanceDepths;
		private IDictionary<string, string> _classInheritanceMap;
		private int _stubbedMethodsCount = 0;

		public LSPExplorer()
		{
			_classInheritanceDepths = new List<int>();
			_classInheritanceMap = new Dictionary<string, string>();
		}

		public void CollectData(SyntaxNode syntaxRoot, SemanticModel semanticModel)
		{
			Task.WaitAll(
				Task.Run(() => UpdateClassInheritanceMap(syntaxRoot, semanticModel)),
				Task.Run(() => CollectInheritanceDepthsOfClasses(syntaxRoot, semanticModel)),
				Task.Run(() => CollectStubbedMethodsCount(syntaxRoot, semanticModel)));
		}

		public void UpdateQuantifiers(Quantifiers quantifiers)
		{
			Task.WaitAll(
			   quantifiers.AddAsync(() =>
					new Quantifier(
						"AverageClassInheritanceDepth",
						() => _classInheritanceDepths.Any() 
							? _classInheritanceDepths.Average() 
							: default(double?))),
			   quantifiers.AddAsync(() =>
					new Quantifier(
						"AverageClassInheritanceDepthToClassCountRatio",
						() => _classInheritanceDepths.Any()
							? _classInheritanceDepths.Average() / _classInheritanceDepths.Count
							: default(double?))),
			   quantifiers.AddAsync(() =>
				   new Quantifier(
					   "MaxClassInheritanceDepth",
					   () => _classInheritanceDepths.Any() 
						? _classInheritanceDepths.Max() 
						: default(double?))),
			   quantifiers.AddAsync(() =>
				   new Quantifier(
					   "AverageClassInheritanceBreadth",
					   () => GetAverageInheritanceBreadth())),
			   quantifiers.AddAsync(() =>
				   new Quantifier(
					   "MaxClassInheritanceBreadth",
					   () => GetMaxInheritanceBreadth())),
				quantifiers.AddAsync(() =>
				   new Quantifier(
					   "StubbedMethodsCount",
					   () => _stubbedMethodsCount)));
		}

		#region Class Inheritance Depth

		private void CollectInheritanceDepthsOfClasses(SyntaxNode documentRoot, SemanticModel documentSemantics)
		{
			if (documentRoot == null)
				throw new ArgumentNullException(nameof(documentRoot));

			if (documentSemantics == null)
				throw new ArgumentNullException(nameof(documentSemantics));

			_classInheritanceDepths.AddRange(
				documentRoot
					.DescendantNodes()
						.OfType<ClassDeclarationSyntax>()
						.Select(classNode => GetInheritanceDepth(classNode, documentSemantics)));
		}

		private int GetInheritanceDepth(ClassDeclarationSyntax classNode, SemanticModel documentSemantics)
		{
			if (classNode == null)
				throw new ArgumentNullException(nameof(classNode));

			if (documentSemantics == null)
				throw new ArgumentNullException(nameof(documentSemantics));

			int CountInheritanceDepth(INamedTypeSymbol type)
			{
				if (type == null) return 0;
				return (type.BaseType == null)
					? 0
					: CountInheritanceDepth(type.BaseType) + 1;
			}

			return CountInheritanceDepth(documentSemantics.GetDeclaredSymbol(classNode) as INamedTypeSymbol);
		}

		#endregion

		#region Abstract Inheritance Breadth

		private void UpdateClassInheritanceMap(SyntaxNode documentRoot, SemanticModel documentSemantics)
		{
			if (documentRoot == null)
				throw new ArgumentNullException(nameof(documentRoot));

			if (documentSemantics == null)
				throw new ArgumentNullException(nameof(documentSemantics));

			foreach (var classType in
				documentRoot
					.DescendantNodes()
					.OfType<ClassDeclarationSyntax>()
					.Select(node => documentSemantics.GetDeclaredSymbol(node) as INamedTypeSymbol))
			{
				// additional check for the ultimate base class (Object),
				// which is not needed on the map
				_classInheritanceMap[classType.ToString()] =
					classType.BaseType.BaseType != null
						? classType.BaseType.ToString()
						: string.Empty;
			}
		}

		private double? GetAverageInheritanceBreadth()
		{
			if (!_classInheritanceMap.Any())
				return default(double?);
			
			return _classInheritanceMap
				.Average(item =>
					string.IsNullOrEmpty(item.Value)
						? 1
						: _classInheritanceMap.Values.Count(v => v == item.Value));
		}

		private double? GetMaxInheritanceBreadth()
		{
			if (!_classInheritanceMap.Any())
				return default(double?);

			return _classInheritanceMap
				.Max(item =>
					string.IsNullOrEmpty(item.Value)
						? 1
						: _classInheritanceMap.Values.Count(v => v == item.Value));
		}

		#endregion

		#region Stubbed Method Count

		private void CollectStubbedMethodsCount(SyntaxNode documentRoot, SemanticModel documentSemantics)
		{
			if (documentRoot == null)
				throw new ArgumentNullException(nameof(documentRoot));

			if (documentSemantics == null)
				throw new ArgumentNullException(nameof(documentSemantics));

			_stubbedMethodsCount +=
				documentRoot.
					DescendantNodes()
						.OfType<MethodDeclarationSyntax>()
						.Where(methodNode =>
							(documentSemantics.GetDeclaredSymbol(methodNode) as IMethodSymbol)
								?.IsOverride
								?? false)
						.Count(methodNode =>
							CheckForNotImplementedExceptions(methodNode, documentSemantics));
		}

		private bool CheckForNotImplementedExceptions(MethodDeclarationSyntax methodNode, SemanticModel documentSemantics)
		{
			if (methodNode == null)
				throw new ArgumentNullException(nameof(methodNode));

			if (documentSemantics == null)
				throw new ArgumentNullException(nameof(documentSemantics));

			var exceptions = methodNode.DescendantNodes().OfType<ThrowStatementSyntax>();

			if (!exceptions.Any())
				return false;

			return exceptions
				.SelectMany( exceptionNode => 
					 exceptionNode
						.DescendantNodes()
						.OfType<ObjectCreationExpressionSyntax>())
				.Any(objectNode =>
					CheckIfObjectIsNotImplementedException(objectNode, documentSemantics));
		}

		private bool CheckIfObjectIsNotImplementedException(
				ObjectCreationExpressionSyntax objectNode,
				SemanticModel semanticModel)
		{
			if (objectNode == null)
				throw new ArgumentNullException(nameof(objectNode));

			if (semanticModel == null)
				throw new ArgumentNullException(nameof(semanticModel));

			return (objectNode.Type.GetText().ToString() == StubExceptionName);

			//todo: it seems there is an issue with roslyn
			// getting the type of exception times out
			// should ask on roslyn github
				
			var exceptionType = semanticModel
				.GetTypeInfo(objectNode.Type)
				.Type;

			bool LookForStubException(ITypeSymbol type)
			{
				if (type == null)
					throw new ArgumentNullException(nameof(type));

				return (type.Name == StubExceptionName)
					? true
					: (type.BaseType == null)
						? false
						: LookForStubException(type.BaseType);
			}

			return LookForStubException(exceptionType);
		}

		#endregion


		
	}
}
