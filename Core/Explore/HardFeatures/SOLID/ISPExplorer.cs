﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Explore
{
	internal class ISPExplorer : IRoslynExplorer
	{
		private readonly IDictionary<string, string> _abstractClassInheritanceMap;
		private readonly List<int> _abstractionWeights;
		private readonly List<int> _abstractInheritanceDepths;
		private readonly List<int> _interfaceCountPerClass;
		private readonly List<int> _extendeesCountPerInterface;
		
		public ISPExplorer()
		{
			_abstractClassInheritanceMap = new Dictionary<string, string>();
			_abstractionWeights = new List<int>();
			_abstractInheritanceDepths = new List<int>();
			_interfaceCountPerClass = new List<int>();
			_extendeesCountPerInterface = new List<int>();
		}

		public void CollectData(SyntaxNode syntaxRoot, SemanticModel semanticModel)
		{
			if (syntaxRoot == null)
				throw new ArgumentNullException(nameof(syntaxRoot));

			if (semanticModel == null)
				throw new ArgumentNullException(nameof(semanticModel));

			Task.WaitAll(
				Task.Run(() => CollectAbstractionWeights(syntaxRoot, semanticModel)),
				Task.Run(() => UpdateAbstractInheritanceMap(syntaxRoot, semanticModel)),
				Task.Run(() => CollectImplementedInterfaceCount(syntaxRoot, semanticModel)),
				Task.Run(() => CollectInterfaceExtendeesCount(syntaxRoot, semanticModel)),
				Task.Run(() => CollectInheritanceDepthsOfAbsractClasses(syntaxRoot, semanticModel)));
		}

		public void UpdateQuantifiers(Quantifiers quantifiers)
		{
			Task.WaitAll(
				quantifiers.AddAsync(() =>
				   new Quantifier(				   
					   "AverageAbstractionWeight",
					   () => _abstractionWeights.Any() 
							? _abstractionWeights.Average() 
							: default(double?))),
			   quantifiers.AddAsync(() =>
				   new Quantifier(
					   "AverageAbstractInheritanceDepth",
					   () => _abstractInheritanceDepths.Any() 
							? _abstractInheritanceDepths.Average() 
							: default(double?))),
			   quantifiers.AddAsync(() =>
				   new Quantifier(
					   "MaxAbstractInheritanceDepth",
					   () => _abstractInheritanceDepths.Any() 
							? _abstractInheritanceDepths.Max() 
							: default(double?))),
			   quantifiers.AddAsync(() =>
				   new Quantifier(
					   "AverageAbstractInheritanceBreadth",
					   () => GetAverageInheritanceBreadth())),
			   quantifiers.AddAsync(() =>
				   new Quantifier(
					   "MaxAbstractInheritanceBreadth",
					   () => GetMaxInheritanceBreadth())),
			   quantifiers.AddAsync(() =>
				   new Quantifier(
					   "AverageInterfaceCountPerClass",
					   () => _interfaceCountPerClass.Any() 
							? _interfaceCountPerClass.Average()
							: default(double?))),
			   quantifiers.AddAsync(() =>
				   new Quantifier(
					   "MaxInterfaceCountPerClass",
					   () => _interfaceCountPerClass.Any() 
							? _interfaceCountPerClass.Max() 
							: default(double?))),
			   quantifiers.AddAsync(() =>
				   new Quantifier(
					   "AverageInterfaceExtendeeCount",
					   () => _extendeesCountPerInterface.Any() 
							? _extendeesCountPerInterface.Average() 
							: default(double?))),
			   quantifiers.AddAsync(() =>
				   new Quantifier(
					   "MaxInterfaceExtendeeCount",
					   () => _extendeesCountPerInterface.Any() 
							? _extendeesCountPerInterface.Max() 
							: default(double?))));
		}

		#region Abstraction Weight

		private void CollectAbstractionWeights(SyntaxNode documentRoot, SemanticModel documentSemantics)
		{
			if (documentRoot == null)
				throw new ArgumentNullException(nameof(documentRoot));

			if (documentSemantics == null)
				throw new ArgumentNullException(nameof(documentSemantics));
			
			_abstractionWeights.AddRange(documentRoot
					.DescendantNodes()
						.OfType<TypeDeclarationSyntax>()
						.Where(node => documentSemantics.GetDeclaredSymbol(node)?.IsAbstract ?? false)
						.Select(LineCounter.GetLinesCountFor));
		}

		#endregion

		#region Abstract Inheritance Depth

		private void CollectInheritanceDepthsOfAbsractClasses(SyntaxNode documentRoot, SemanticModel documentSemantics)
		{
			if (documentRoot == null)
				throw new ArgumentNullException(nameof(documentRoot));

			if (documentSemantics == null)
				throw new ArgumentNullException(nameof(documentSemantics));

			_abstractInheritanceDepths.AddRange(
				documentRoot
					.DescendantNodes()
						.OfType<ClassDeclarationSyntax>()
						.Where(classNode => documentSemantics.GetDeclaredSymbol(classNode)?.IsAbstract ?? false)
						.Select(classNode => GetAbstractInheritanceDepth(classNode, documentSemantics)));
		}

		private int GetAbstractInheritanceDepth(ClassDeclarationSyntax classNode, SemanticModel documentSemantics)
		{
			if (classNode == null)
				throw new ArgumentNullException(nameof(classNode));

			if (documentSemantics == null)
				throw new ArgumentNullException(nameof(documentSemantics));

			int CountInheritanceDepth(INamedTypeSymbol type)
			{
				if (type == null || !type.IsAbstract) return 0;
				return (type.BaseType == null)
					? 0
					: CountInheritanceDepth(type.BaseType) + 1;
			}

			return CountInheritanceDepth(documentSemantics.GetDeclaredSymbol(classNode) as INamedTypeSymbol);
		}

		#endregion

		#region Implemented Interfaces Count

		private void CollectImplementedInterfaceCount(SyntaxNode documentRoot, SemanticModel documentSemantics)
		{
			if (documentRoot == null)
				throw new ArgumentNullException(nameof(documentRoot));

			if (documentSemantics == null)
				throw new ArgumentNullException(nameof(documentSemantics));

			_interfaceCountPerClass.AddRange(
				documentRoot
					.DescendantNodes()
						.OfType<ClassDeclarationSyntax>()
						.Select(classNode =>
							(documentSemantics.GetDeclaredSymbol(classNode) as INamedTypeSymbol)
								?.Interfaces.Count()
								?? 0)
						.ToArray());
		}

		#endregion

		#region Abstract Inheritance Breadth

		private void UpdateAbstractInheritanceMap(SyntaxNode documentRoot, SemanticModel documentSemantics)
		{
			if (documentRoot == null)
				throw new ArgumentNullException(nameof(documentRoot));

			if (documentSemantics == null)
				throw new ArgumentNullException(nameof(documentSemantics));

			foreach (var classType in 
				documentRoot
					.DescendantNodes()
					.OfType<ClassDeclarationSyntax>()
					.Select(node => documentSemantics.GetDeclaredSymbol(node) as INamedTypeSymbol)
					.Where(symbol => symbol.IsAbstract))
			{
				// additional check for the ultimate base class (Object),
				// which is not needed on the map
				_abstractClassInheritanceMap[classType.ToString()] =
					classType.BaseType.BaseType != null
						? classType.BaseType.ToString()
						: string.Empty;
			}
		}

		private double? GetAverageInheritanceBreadth()
		{
			if (!_abstractClassInheritanceMap.Any())
				return default(double?);

			return _abstractClassInheritanceMap
				.Average(item =>
					string.IsNullOrEmpty(item.Value) 
						? 1
						: _abstractClassInheritanceMap.Values.Count(v => v == item.Value));
		}

		private double? GetMaxInheritanceBreadth()
		{
			if (!_abstractClassInheritanceMap.Any())
				return default(double?);
			
			return _abstractClassInheritanceMap
				.Max(item =>
					string.IsNullOrEmpty(item.Value)
						? 1
						: _abstractClassInheritanceMap.Values.Count(v => v == item.Value));
		}

		#endregion

		#region Interface Extension

		private void CollectInterfaceExtendeesCount(SyntaxNode documentRoot, SemanticModel documentSemantics)
		{
			if (documentRoot == null)
				throw new ArgumentNullException(nameof(documentRoot));

			if (documentSemantics == null)
				throw new ArgumentNullException(nameof(documentSemantics));

			_extendeesCountPerInterface.AddRange(
				documentRoot
					.DescendantNodes()
						.OfType<InterfaceDeclarationSyntax>()
						.Select(interfaceNode =>
							(documentSemantics.GetDeclaredSymbol(interfaceNode) as INamedTypeSymbol)
								?.AllInterfaces.Count()
								?? 0)
						.ToArray());
		}

		#endregion
	}
}
