﻿using Core.Auxiliary;
using Core.Explore;
using Core.Extensions;
using Octokit;
using System;
using System.Linq;
using System.Threading.Tasks;
using NLog;

namespace Core
{
	public static class ExtractorFactory
	{
		private const string ExtractorHeader = "rc-extractor";
		
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

		public static async Task<Extractor> GetExtractorFor(string repositoryUrl)
		{
			if (string.IsNullOrWhiteSpace(repositoryUrl)) throw new ArgumentNullException(nameof(repositoryUrl));

			var ownerAndRepo = RepoUrlParser.GetOwnerAndRepoFromUrl(repositoryUrl);

			return await GetExtractorFor(ownerAndRepo.owner, ownerAndRepo.repo);
		}

		public static async Task<Extractor> GetExtractorFor(string owner, string name)
		{
			if (string.IsNullOrWhiteSpace(owner)) throw new ArgumentNullException(nameof(owner));
			if (string.IsNullOrWhiteSpace(name)) throw new ArgumentNullException(nameof(name));

			Logger.Trace($"Initialising extractor for \"{owner}\\{name}\"...");
			var gitHubClient = new GitHubClient(new ProductHeaderValue(ExtractorHeader), new DefaultCredentialStore());
			var repository = await gitHubClient.Repository.Get(owner, name);
			var localRepository = await repository.GetLocalAsync();

			var containerTask = SolutionContainer.Instance(await repository.GetLocalAsync());
			var databaseTask = PrepareDatabaseFor(repository, localRepository, gitHubClient);

			await Task.WhenAll(containerTask, databaseTask);
			Logger.Info($"Extractor initialised for \"{owner}\\{name}\".");

			return new Extractor(gitHubClient, repository, containerTask.Result);
		}

		private static async Task PrepareDatabaseFor(Repository repository, LibGit2Sharp.Repository localRepository, GitHubClient gitHubClient)
		{
			using (var context = new Data.DataContext())
			{
				context.Database.Initialize(false);
			}

			var populateCommitsTask = PopulateCommits(localRepository);
			var populateIssuesTask = PopulateIssues(repository, gitHubClient);

			await Task.WhenAll(populateCommitsTask, populateIssuesTask);
		}

		private static async Task PopulateCommits(LibGit2Sharp.Repository repository)
		{
			if (repository == null) throw new ArgumentNullException(nameof(repository));

			var localCommits = repository.Commits.Where(commit => commit.Parents.Count() <= 1);
			var commits = localCommits
				.Select(commit => new Data.Models.Commit().FromLibGitCommit(repository, commit));

			using (var context = new Data.DataContext())
			{				
				context.Commits.AddRange(commits);
				await context.SaveChangesAsync();
			}
		}

		private static async Task PopulateIssues(Repository repository, GitHubClient gitHubClient)
		{
			if (repository == null) throw new ArgumentNullException(nameof(repository));
			if (gitHubClient == null) throw new ArgumentNullException(nameof(gitHubClient));

			var octokitIssues = await gitHubClient.Issue
				.GetAllForRepository(repository.Id, new RepositoryIssueRequest() { State = ItemStateFilter.All });
			var issues = octokitIssues.Select(issue => new Data.Models.Issue().FromOctokitIssue(issue));

			using (var context = new Data.DataContext())
			{
				context.Issues.AddRange(issues);
				await context.SaveChangesAsync();
			}
		}
	}
}
