﻿using LibGit2Sharp;
using System;
using System.Linq;

namespace Core.Extensions
{
    internal static class CommitExtenstions
    {
        internal static Data.Models.Commit FromLibGitCommit(
            this Data.Models.Commit dbCommit,
            Repository repository,
            Commit commit)
        {
            if (dbCommit == null) throw new ArgumentNullException(nameof(dbCommit));
            if (repository == null) throw new ArgumentNullException(nameof(repository));
            if (commit == null) throw new ArgumentNullException(nameof(commit));

            if (commit.Parents.Count() > 1)
                throw new ArgumentException($"Commit must have one or no parents (Sha: {commit.Sha}).");
                       
            var diff = repository.Diff.Compare<Patch>(commit.Parents.SingleOrDefault()?.Tree ?? default(Tree), commit.Tree);

            dbCommit.Sha = commit.Sha;
            dbCommit.AuthorEmail = commit.Author?.Email;
            dbCommit.AuthorName = commit.Author?.Name;
            dbCommit.Message = commit.Message;
            dbCommit.Additions = diff?.LinesAdded ?? 0;
            dbCommit.Deletions = diff?.LinesDeleted ?? 0;
            dbCommit.When = commit.Committer.When.DateTime;

            return dbCommit;
        }
    }
}
