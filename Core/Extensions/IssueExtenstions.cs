﻿using Octokit;
using System;
using System.Linq;

namespace Core.Extensions
{
	internal static class IssueExtenstions
	{
		internal static Data.Models.Issue FromOctokitIssue(this Data.Models.Issue dbIssue, Issue issue)
		{
			if (dbIssue == null) throw new ArgumentNullException(nameof(dbIssue));
			if (issue == null) throw new ArgumentNullException(nameof(issue));

			dbIssue.Title = issue.Title;
			dbIssue.Body = issue.Body;
			dbIssue.CreatedAt = issue.CreatedAt.UtcDateTime;
			dbIssue.ClosedAt = issue.ClosedAt?.UtcDateTime;
			dbIssue.DaysSurvived = ((issue.ClosedAt?.UtcDateTime ?? DateTime.UtcNow) - issue.CreatedAt.UtcDateTime).Days;

			return dbIssue;
		}
	}
}
