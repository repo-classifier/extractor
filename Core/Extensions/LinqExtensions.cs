﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Core.Extensions
{
	public static class LinqExtensions
	{
		public static double StandardDeviation<T>(this IQueryable<T> list, Expression<Func<T, int>> selector)
		{
			var sumOfSquaresOfDifferences = list
				.Select(selector)
				.Sum(item => Math.Pow((item) - list.Average(selector), 2));

			return Math.Sqrt(sumOfSquaresOfDifferences / list.Count());
		}

		public static double StandardDeviation<T>(this IQueryable<T> list, Expression<Func<T, double>> selector)
		{
			var sumOfSquaresOfDifferences = list
				.Select(selector)
				.Sum(item => Math.Pow((item) - list.Average(selector), 2));

			return Math.Sqrt(sumOfSquaresOfDifferences / list.Count());
		}
	}
}
