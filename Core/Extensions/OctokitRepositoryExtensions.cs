﻿using Core.Auxiliary;
using System;
using System.Threading.Tasks;

namespace Core.Extensions
{
    public static class OctokitRepositoryExtensions
    {
        public static async Task<LibGit2Sharp.Repository> GetLocalAsync(this Octokit.Repository repository)
        {
            if (repository == null) throw new ArgumentNullException(nameof(repository));

            return await LocalRepositories.GetLocalAsync(repository);
        }
    }
}
