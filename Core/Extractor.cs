﻿using Core.Explore;
using Core.Explore.ContinuousIntegration;
using Core.Explore.License;
using Octokit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Auxiliary;
using Core.Extensions;
using NLog;
using System.Runtime.CompilerServices;

namespace Core
{
	public sealed class Extractor
	{
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

		private IGitHubClient _gitHubClient;
		private Repository _repository;
		private ISolutionContainer _solutionContainer;

		private RoslynExtractor _roslynExtractor;
		private ICollection<IExplorer> _explorers;
		
		internal Extractor(
			GitHubClient gitHubClient,
			Repository repository,
			ISolutionContainer solutionContainer)
		{
			var config = ExtractorConfiguration.GetConfiguration();
						
			_gitHubClient = gitHubClient ?? throw new ArgumentNullException(nameof(gitHubClient));
			_repository = repository ?? throw new ArgumentNullException(nameof(repository));
			_solutionContainer = solutionContainer ?? throw new ArgumentNullException(nameof(solutionContainer));


			_explorers = new List<IExplorer>
			{
				new LicenseExplorer(_repository),
				new ContinuousIntegrationExplorer(_gitHubClient, _repository),
				new IssueExplorer(),
				new PopularityExplorer(_repository),
				new LifespanExplorer(
					config.CommitMessageClassificationVocabulary.Adaptive.Keywords,
					config.CommitMessageClassificationVocabulary.Corrective.Keywords,
					config.CommitMessageClassificationVocabulary.Perfective.Keywords)
			};
			
			_roslynExtractor = RoslynExtractor
				.Initialize(_solutionContainer)
				.With(new TestRatioExplorer(
					config.TestingAttributesVocabulary.ClassAttributes.Keywords,
					config.TestingAttributesVocabulary.MethodAttributes.Keywords))
				.With(new CommentRatioExplorer())
				.With(new ISPExplorer())
				.With(new DIPExplorer())
				.With(new LSPExplorer())
				.With(new OCPExplorer(_repository))
				.With(new SRPExporer())
				.Build();			
		}

		public async Task<IEnumerable<Quantifier>> ExtractQuantifiers()
		{
			var quantifiers = new Quantifiers();

			Logger.Trace("Extracting soft quantifiers...");
			Parallel.ForEach(_explorers, explorer => explorer.UpdateQuantifiers(quantifiers));
			Logger.Info("Soft quantifiers extracted.");

			_roslynExtractor.DoExtractionForEachDocument().Wait();
			
			var rateLimit = _gitHubClient.GetLastApiInfo().RateLimit;
			Logger.Warn($">> Remaining rate limit ({rateLimit.Remaining}) will be reset at {rateLimit.Reset.LocalDateTime}. <<");

			var result = quantifiers
				.Concat(_roslynExtractor.Quantifiers)
				.OrderBy(quantifier => quantifier.Name)
				.ToArray();

			await CleanUp();

			return result;
		}

		private async Task CleanUp()
		{
			Logger.Info("Cleaning up...");

			Logger.Trace("Purging database...");
			using (var context = new Data.DataContext())
			{
				context.TruncateCommits();
				context.TruncateIssues();
			}

			Logger.Trace("Disposing local repository...");
			var localRepo = await _repository.GetLocalAsync();
			var repoPath = localRepo.Info.WorkingDirectory;
			localRepo.Dispose();

			_gitHubClient = null;
			_solutionContainer = null;
			_repository = null;
			_explorers = null;
			_roslynExtractor = null;

			GC.Collect();
			GC.WaitForPendingFinalizers();
			
			try
			{
				Logger.Trace("Deleting local repository directory...");
				DirectoryHelper.Delete(repoPath);
			}
			catch (Exception exception)
			{
				Logger.Error($"Failed to delete repository's directory! {Environment.NewLine}" +
				             $"{exception}");
			}

			Logger.Info("Clean up complete!");
		}

		
	}
}
