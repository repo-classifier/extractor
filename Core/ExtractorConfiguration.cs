﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;

namespace Core
{
	internal class ExtractorConfiguration : ConfigurationSection
	{
		private const string ExtractorConfigurationName = "extractorConfiguration";
		private const string RepositoryCacheDirectoryName = "repositoryCacheDirectory";
		private const string CommitMessageClassificationVocabularyName = "commitMessageClassificationVocabulary";
		private const string TestingAttributesVocabularyName = "testingAttributesVocabulary";

		private static readonly Lazy<ExtractorConfiguration> _instance =
			new Lazy<ExtractorConfiguration>(() => 
				(ExtractorConfiguration)ConfigurationManager.GetSection(ExtractorConfigurationName));

		private ExtractorConfiguration()
		{

		}

		public static ExtractorConfiguration GetConfiguration()
		{
			return _instance.Value 
				?? throw new InvalidOperationException(
					$"Configuration file does not contain <{ExtractorConfigurationName}> section!");
		}

		[ConfigurationProperty(RepositoryCacheDirectoryName, IsRequired = true)]
		public RepositoryCacheDirectoryElement RepositoryCacheDirectory
		{
			get => (RepositoryCacheDirectoryElement)this[RepositoryCacheDirectoryName];
		}

		[ConfigurationProperty(CommitMessageClassificationVocabularyName, IsRequired = true)]
		public CommitMessageClassificationVocabularyElement CommitMessageClassificationVocabulary
		{
			get => (CommitMessageClassificationVocabularyElement)this[CommitMessageClassificationVocabularyName];
		}

		[ConfigurationProperty(TestingAttributesVocabularyName, IsRequired = true)]
		public TestingAttributesVocabularyElement TestingAttributesVocabulary
		{
			get => (TestingAttributesVocabularyElement)this[TestingAttributesVocabularyName];
		}

		#region Repository Cache Directory Element Definition

		public class RepositoryCacheDirectoryElement : ConfigurationElement
		{
			private const string PathName = "path";

			[ConfigurationProperty(PathName, IsRequired = true)]
			public string Path
			{
				get => (string)base[PathName];
			}
		}

		#endregion

		#region Keywords Element Definition

		public class KeywordsElement : ConfigurationElement
		{
			private const string KeywordsName = "keywords";

			[ConfigurationProperty(KeywordsName, IsRequired = true)]
			[TypeConverter(typeof(CommaDelimitedStringCollectionConverter))]
			public IEnumerable<string> Keywords
			{
				get => ((CommaDelimitedStringCollection)base[KeywordsName]).Cast<string>();
			}
		}

		#endregion

		#region Commit Message Classification Vocabulary Element Definition

		public class CommitMessageClassificationVocabularyElement : ConfigurationElement
		{
			private const string AdaptiveName = "adaptive";
			private const string CorreciveName = "corrective";
			private const string PerfectiveName = "perfective";

			[ConfigurationProperty(AdaptiveName, IsRequired = true)]
			public KeywordsElement Adaptive
			{
				get => (KeywordsElement)this[AdaptiveName];
			}

			[ConfigurationProperty(CorreciveName, IsRequired = true)]
			public KeywordsElement Corrective
			{
				get => (KeywordsElement)this[CorreciveName];
			}

			[ConfigurationProperty(PerfectiveName, IsRequired = true)]
			public KeywordsElement Perfective
			{
				get => (KeywordsElement)this[PerfectiveName];
			}
		}

		#endregion

		#region Testing Attributes Element Vocabulary Definition

		public class TestingAttributesVocabularyElement : ConfigurationElement
		{
			private const string ClassAttributesName = "classAttributes";
			private const string MethodAttributesName = "methodAttributes";

			[ConfigurationProperty(ClassAttributesName, IsRequired = true)]
			public KeywordsElement ClassAttributes
			{
				get => (KeywordsElement)this[ClassAttributesName];
			}

			[ConfigurationProperty(MethodAttributesName, IsRequired = true)]
			public KeywordsElement MethodAttributes
			{
				get => (KeywordsElement)this[MethodAttributesName];
			}
		}

		#endregion
				
	}
}
