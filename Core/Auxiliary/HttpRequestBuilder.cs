﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Core.Auxiliary
{
    internal class HttpRequestBuilder
    {
        private Uri _requestUrl;
        private string _requestPath;

        private string _acceptHeader;
        private string _userAgentHeader;
        private Dictionary<string, string> _parameters;

        internal HttpRequestBuilder()
        {
            _requestPath = string.Empty;
            _parameters = new Dictionary<string, string>();
        }

        internal HttpRequestBuilder WithUrl(string url)
        {
            if (string.IsNullOrWhiteSpace(url)) throw new ArgumentNullException(nameof(url));

            _requestUrl = new Uri(url);
            return this;
        }

        internal HttpRequestBuilder WithPath(string path)
        {
            if (string.IsNullOrWhiteSpace(path)) throw new ArgumentNullException(nameof(path));

            _requestPath = Path.Combine(_requestPath, path);
            return this;
        }

        internal HttpRequestBuilder WithAcceptHeader(string acceptHeader)
        {
            if (string.IsNullOrWhiteSpace(acceptHeader)) throw new ArgumentNullException(nameof(acceptHeader));

            _acceptHeader = acceptHeader;
            return this;
        }

        internal HttpRequestBuilder WithUserAgentHeader(string userAgentHeader)
        {
            if (string.IsNullOrWhiteSpace(userAgentHeader)) throw new ArgumentNullException(nameof(userAgentHeader));

            _userAgentHeader = userAgentHeader;
            return this;
        }

        internal HttpRequestBuilder WithParameter(string name, string value)
        {
            if (string.IsNullOrWhiteSpace(name)) throw new ArgumentNullException(nameof(name));
            if (string.IsNullOrWhiteSpace(value)) throw new ArgumentNullException(nameof(value));

            _parameters[name] = value;
            return this;
        }

        internal HttpRequestBuilder WithOptionalParameter(string name, string value)
        {
            if (string.IsNullOrWhiteSpace(name)) return this;
            if (string.IsNullOrWhiteSpace(value)) return this;

            _parameters[name] = value;
            return this;
        }

        internal HttpWebRequest Build()
        {
            if (string.IsNullOrWhiteSpace(_requestPath)) throw new ArgumentNullException(nameof(_requestPath));

            if (_parameters?.Count > 0)
            {
                _requestPath += "?";
                foreach (var parameter in _parameters
                    .Select((parameter, index) => new
                    {
                        Index = index,
                        Name = parameter.Key,
                        Value = parameter.Value
                    }))
                {
                    _requestPath += (parameter.Index == 0 ? string.Empty : "&") + $"{parameter.Name}={parameter.Value}";
                }
            }

            var request = (HttpWebRequest)WebRequest.Create(new Uri(_requestUrl, _requestPath));

            if (!string.IsNullOrWhiteSpace(_acceptHeader)) request.Accept = _acceptHeader;
            if (!string.IsNullOrWhiteSpace(_userAgentHeader)) request.UserAgent = _userAgentHeader;

            return request;
        }

        internal async Task<string> BuildAndFetchResponseAsync()
        {
            var request = this.Build();
            var response = await request.GetResponseAsync();

            string responseStream;
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                responseStream = await reader.ReadToEndAsync();
            }

            return responseStream;
        }

        internal string BuildAndFetchResponse()
        {
            var request = Build();
            var response = request.GetResponse();

            string responseStream;
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                responseStream = reader.ReadToEnd();
            }

            return responseStream;
        }
    }
}
