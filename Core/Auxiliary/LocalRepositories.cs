﻿using LibGit2Sharp;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using System;

namespace Core.Auxiliary
{
    public static class LocalRepositories
    {
        public static async Task<string> Clone(Octokit.Repository repository)
        {
            if (repository == null) throw new ArgumentNullException(nameof(repository));

            var repositoryPath = MakeRepoPath(repository);

            return Repository.IsValid(repositoryPath)
                 ? repositoryPath
                 : await Task.Run(() => Repository.Clone(repository.CloneUrl, repositoryPath));
        }

        public static async Task<Repository> GetLocalAsync(Octokit.Repository repository)
        {
            if (repository == null) throw new ArgumentNullException(nameof(repository));

            var repositoryPath = MakeRepoPath(repository);

            return Repository.IsValid(repositoryPath)
                ? new Repository(repositoryPath)
                : new Repository(await Clone(repository));
        }

        private static string MakeRepoPath(Octokit.Repository repository)
        {
            if (repository == null) throw new ArgumentNullException(nameof(repository));

            return Path.Combine(GetLocalDirPath(), repository.Name);
        }

        private static string GetLocalDirPath()
        {
            return ExtractorConfiguration.GetConfiguration().RepositoryCacheDirectory.Path;
        }

    }
}
