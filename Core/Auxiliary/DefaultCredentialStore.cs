﻿using System;
using System.Threading.Tasks;
using Octokit;

namespace Core.Auxiliary
{
    internal class DefaultCredentialStore : ICredentialStore
    {
        public Task<Credentials> GetCredentials()
        {
            return Task.Run(() => new Credentials("rc-extractor", "ilovemsr1"));
        }       
    }
}
