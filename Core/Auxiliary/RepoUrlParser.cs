﻿using System;
using System.Text.RegularExpressions;

namespace Core.Auxiliary
{
	internal static class RepoUrlParser
	{
		private const string UrlAndRepoPattern =
		   @"(?<host>(git@|http(s)?://)([\w.@]+)(/|:))(?<owner>[\w-_]+)/(?<repo>[\w-_.]+)(.git){0,1}((/){0,1})";

		private const string dotGitExtension = @".git$";

		internal static (string owner, string repo) GetOwnerAndRepoFromUrl(string url)
		{
			if (string.IsNullOrEmpty(url)) throw new ArgumentNullException(nameof(url));

			//remove .git at the end of input url
			url = Regex.Replace(url, ".git$", "");

			Match match = Regex.Match(url, UrlAndRepoPattern);
			
			return match.Success
				? (match.Groups["owner"].Value, match.Groups["repo"].Value)
				: throw new FormatException(nameof(url));
		}
	}
}
