﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Auxiliary
{
	internal static class MathHelper
	{
		public static double GetProportion(double value, double total)
		{
			if (value == 0) return 0;
			if (total == 0) return 1;

			return value / total;
		}
	}
}
