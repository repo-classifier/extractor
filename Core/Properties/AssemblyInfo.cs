﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Core")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Core")]
[assembly: AssemblyCopyright("Copyright ©  2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("bf9217b7-7c40-4ebf-a4e7-b67e03fd3abb")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

[assembly: InternalsVisibleTo("Core.Tests,PublicKey=" +
	"00240000048000009400000006020000002400005253413100040000010001003ff4aa0d1eee9f"+
	"bef1afa3f9ce8a9074e4d9e70c2f66aaafedd95aa9e8becf55fd084649adeb5ca2401b56a2025b"+
	"b2eab0bc9c41cb59d98483795d2aefc93afe8d72dfa4fc37671acb0d9a1cc352c7c7de9e06c9f8"+
	"5549335bcb74a065f8451bddbb3f01679b600534c0040f86a4f661d3d5db48abc7e6cb321fbd45"+
	"03d656d7")]
