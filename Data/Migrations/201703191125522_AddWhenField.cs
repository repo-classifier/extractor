namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddWhenField : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Commits", "When", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Commits", "When");
        }
    }
}
