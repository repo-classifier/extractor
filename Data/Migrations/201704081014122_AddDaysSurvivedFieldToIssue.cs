namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDaysSurvivedFieldToIssue : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Issues", "DaysSurvived", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Issues", "DaysSurvived");
        }
    }
}
