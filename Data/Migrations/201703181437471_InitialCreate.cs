namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Commits",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Sha = c.String(),
                        AuthorName = c.String(),
                        AuthorEmail = c.String(),
                        Message = c.String(),
                        Additions = c.Int(nullable: false),
                        Deletions = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Commits");
        }
    }
}
