using Data.Models;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace Data
{
	public class DataContext : DbContext
	{
		public DataContext()
			: base("name=ExtractorData")
		{
			Database.SetInitializer(new CreateDatabaseIfNotExists<DataContext>());
		}

		public virtual DbSet<Commit> Commits { get; set; }
		public virtual DbSet<Issue> Issues { get; set; }

		public void TruncateCommits()
		{
			Database.ExecuteSqlCommand("TRUNCATE TABLE Commits");
		}

		public void TruncateIssues()
		{
			Database.ExecuteSqlCommand("TRUNCATE TABLE Issues");
		}
	}
}