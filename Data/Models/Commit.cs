﻿using System;

namespace Data.Models
{
    public class Commit : BaseDataModel
    {
        public string Sha { get; set; }
        public string AuthorName { get; set; }
        public string AuthorEmail { get; set; }
        public string Message { get; set; }
        public int Additions { get; set; }
        public int Deletions { get; set; }
        public DateTime When { get; set; }
    }

}
