﻿using System;

namespace Data.Models
{
	public class Issue : BaseDataModel
	{
		public DateTime CreatedAt { get; set; }
		public DateTime? ClosedAt { get; set; }
		public string Body { get; set; }
		public string Title { get; set; }
		public int DaysSurvived { get; set; }
	}	
}
